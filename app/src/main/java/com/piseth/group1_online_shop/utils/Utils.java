package com.piseth.group1_online_shop.utils;

import com.piseth.group1_online_shop.api.PofApis;
import com.piseth.group1_online_shop.api.PofClients;

public class Utils {
    public static PofApis getPofApis() {
        return PofClients.getPofClient().create(PofApis.class);
    }

}

