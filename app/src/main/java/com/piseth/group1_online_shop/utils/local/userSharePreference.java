package com.piseth.group1_online_shop.utils.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.piseth.group1_online_shop.models.User;

public class userSharePreference {
    public static void setUser(Context context, User data){
        SharedPreferences.Editor sharedPreferences=context.getSharedPreferences("USER",Context.MODE_PRIVATE).edit();
        sharedPreferences.putInt("ID", data.getId());
        sharedPreferences.putString("USERNAME", data.getUsername());
        sharedPreferences.putString("EMAIL", data.getEmail());
        sharedPreferences.putString("ACCESS_TOKEN", data.getAccessToken());
        sharedPreferences.putString("REFRESH_TOKEN", data.getRefreshToken());
        sharedPreferences.apply();
    }
    public static User getUser(Context context){
        SharedPreferences preferences=context.getSharedPreferences("USER",Context.MODE_PRIVATE);
        User user=new User();
        user.setId(preferences.getInt("ID",0));
        user.setUsername(preferences.getString("USERNAME",""));
        user.setEmail(preferences.getString("EMAIL", ""));
        user.setAccessToken(preferences.getString("ACCESS_TOKEN", ""));
        user.setRefreshToken(preferences.getString("REFRESH_TOKEN", ""));
        return user;
    }
    public static void removeUser(Context context){
        SharedPreferences.Editor sharedPreferences=context.getSharedPreferences("USER",Context.MODE_PRIVATE).edit();
        sharedPreferences.remove("ID");
        sharedPreferences.remove("USERNAME");
        sharedPreferences.remove("EMAIL");
        sharedPreferences.remove("ACCESS_TOKEN");
        sharedPreferences.remove("REFRESH_TOKEN");
        sharedPreferences.apply();
    }
}
