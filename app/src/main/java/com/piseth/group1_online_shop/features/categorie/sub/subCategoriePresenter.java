package com.piseth.group1_online_shop.features.categorie.sub;

import com.piseth.group1_online_shop.models.res.SubCategorie;
import com.piseth.group1_online_shop.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class subCategoriePresenter {
    private subCategorieView view;

    public subCategoriePresenter(subCategorieView view) {
        this.view = view;
    }

    public void getAllSubCategorie(Integer id){
      view.onLoading("");
        Call<List<SubCategorie>> call= Utils.getPofApis().getAllSubCategories(id);
        call.enqueue(new Callback<List<SubCategorie>>() {
            @Override
            public void onResponse(Call<List<SubCategorie>> call, Response<List<SubCategorie>> response) {
                view.onHideLoading("");
                if (response.isSuccessful() && null !=response.body()){
                    view.onSuccess("Data success");
                    view.setDataSubCategorie(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<SubCategorie>> call, Throwable t) {
                view.onHideLoading("");
                view.onInternalServerError("Connection timeout");
            }
        });
     }
}
