package com.piseth.group1_online_shop.features.home;

import com.piseth.group1_online_shop.models.res.Category;
import com.piseth.group1_online_shop.models.res.Slide;
import com.piseth.group1_online_shop.view.BaseView;

import java.util.List;

public interface HomeView extends BaseView {
    void setCategoryList(List<Category> categoryList);
    void setSlideList(List<Slide> slideList);
}
