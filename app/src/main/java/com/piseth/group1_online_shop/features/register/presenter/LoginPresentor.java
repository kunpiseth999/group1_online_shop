package com.piseth.group1_online_shop.features.register.presenter;


import com.piseth.group1_online_shop.features.register.view.LoginView;
import com.piseth.group1_online_shop.models.User;
import com.piseth.group1_online_shop.models.req.LoginReq;
import com.piseth.group1_online_shop.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresentor {
    private LoginView loginView;

    public LoginPresentor(LoginView loginView) {
        this.loginView = loginView;
    }
    public void login(String userName,String password){
        loginView.onLoading("Loading....");
        LoginReq req=new LoginReq();
        req.setPhone(userName);
        req.setPassword(password);
        Call<User> loginCall= Utils.getPofApis().authLogin(req);
        loginCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
               loginView.onHideLoading("Hiding....");
               if (response.code()== 401){
                   loginView.onError("username and password incorrect!!!");
               }
               if (response.isSuccessful() && null != response.body()){
                   loginView.onSuccess("Welcome");
                   loginView.onLoginSuccess(response.body());
               }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                loginView.onHideLoading("Hide");
                loginView.onInternalServerError("connection time out!!!");
            }
        });
    }
}
