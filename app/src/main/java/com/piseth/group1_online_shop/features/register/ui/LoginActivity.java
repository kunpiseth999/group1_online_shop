package com.piseth.group1_online_shop.features.register.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.piseth.group1_online_shop.R;
import com.piseth.group1_online_shop.app.BaseActivity;
import com.piseth.group1_online_shop.features.register.presenter.LoginPresentor;
import com.piseth.group1_online_shop.features.register.view.LoginView;
import com.piseth.group1_online_shop.models.User;
import com.piseth.group1_online_shop.utils.local.userSharePreference;

public class LoginActivity extends BaseActivity implements LoginView {
    private ProgressBar progressBar;
    private EditText etUserName,etPassword;
    private Button btnLogin;
    private LoginPresentor loginPresentor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        initView();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName=etUserName.getText().toString().trim();
                String password=etPassword.getText().toString().trim();
                if (userName.equals("")) {
                    showMessage("Username is empty!!!");
                    return;
                }
                if (password.equals("")){
                    showMessage("Password is empty!!!");
                    return;
                }
                    loginPresentor.login(userName, password);
            }
        });
    }
    private void initView(){
        loginPresentor=new LoginPresentor(this);
        progressBar=findViewById(R.id.progressBarLogin);
        etUserName=findViewById(R.id.etLoginUsername);
        etPassword=findViewById(R.id.etLoginPassword);
        btnLogin=findViewById(R.id.btnLogin);
    }
    @Override
    public void onLoginSuccess(User user) {
        userSharePreference.setUser(LoginActivity.this,user);
        finish();
    }

    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        showMessage(message);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(String message) {
        showMessage(message);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onInternalServerError(String message) {
        showMessage(message);
        progressBar.setVisibility(View.GONE);
    }
}
