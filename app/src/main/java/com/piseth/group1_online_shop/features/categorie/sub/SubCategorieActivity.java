package com.piseth.group1_online_shop.features.categorie.sub;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.piseth.group1_online_shop.R;
import com.piseth.group1_online_shop.models.res.SubCategorie;

import java.util.List;
import com.piseth.group1_online_shop.adapters.subCategoryAdapter;
public class SubCategorieActivity extends AppCompatActivity implements subCategorieView{
private RecyclerView recyclerViewSubCategories;
private ProgressBar progressBar;
private subCategoryAdapter SubCategoryAdapter;
private subCategoriePresenter categoriePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categorie);
        recyclerViewSubCategories=findViewById(R.id.recylerViewSubCategories);
        progressBar=findViewById(R.id.progressBar);
        categoriePresenter=new subCategoriePresenter(this);
        Intent intent=getIntent();
        Integer id= intent.getIntExtra("ID",0);
        if (id!=0){
            categoriePresenter.getAllSubCategorie(id);
        }
    }

    @Override
    public void setDataSubCategorie(List<SubCategorie> subCategorieList) {
        if (!subCategorieList.isEmpty()){
            SubCategoryAdapter=new subCategoryAdapter(this,subCategorieList);
            recyclerViewSubCategories.setLayoutManager(new GridLayoutManager(this,4));
            recyclerViewSubCategories.setAdapter(SubCategoryAdapter);
        }
    }

    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onSuccess(String message) {

    }

    @Override
    public void onInternalServerError(String message) {

    }
}