package com.piseth.group1_online_shop.features.home;

import com.piseth.group1_online_shop.models.res.Category;
import com.piseth.group1_online_shop.models.res.Slide;
import com.piseth.group1_online_shop.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresentor {
    private HomeView view;

    public HomePresentor(HomeView view) {
        this.view = view;
    }
    public void getAllCategorys(){
        view.onLoading("");
        Call<List<Category>> categoryResCall= Utils.getPofApis().getAllCategory();
        categoryResCall.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                view.onHideLoading("");
                if (response.isSuccessful() && null != response.body()){
                    view.onSuccess("get data success");
                    view.setCategoryList(response.body());
                }else {
                    view.onError("get data unsuccess");
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                view.onHideLoading("");
                view.onHideLoading("connection timeout");
            }
        });
    }
    public void getAllSlide(){
        view.onHideLoading("");
        Call<List<Slide>> slideResCall=Utils.getPofApis().getAllSlide();
        slideResCall.enqueue(new Callback<List<Slide>>() {
            @Override
            public void onResponse(Call<List<Slide>> call, Response<List<Slide>> response) {
                view.onHideLoading("");
                if (response.isSuccessful() && null!= response.body()){
                    view.setSlideList(response.body());
                }else {
                    view.onError("Error get all slide");
                }
            }

            @Override
            public void onFailure(Call<List<Slide>> call, Throwable t) {
                view.onHideLoading("");
                view.onInternalServerError("Connection timeout !"+t.getLocalizedMessage());
            }
        });
    }
}
