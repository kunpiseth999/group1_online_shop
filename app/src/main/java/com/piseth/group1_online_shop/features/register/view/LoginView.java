package com.piseth.group1_online_shop.features.register.view;

import com.piseth.group1_online_shop.models.User;
import com.piseth.group1_online_shop.view.BaseView;

public interface LoginView extends BaseView {
   void onLoginSuccess(User user);
}
