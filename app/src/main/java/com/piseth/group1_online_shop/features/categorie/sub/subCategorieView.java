package com.piseth.group1_online_shop.features.categorie.sub;

import com.piseth.group1_online_shop.models.res.SubCategorie;
import com.piseth.group1_online_shop.view.BaseView;

import java.util.List;

public interface subCategorieView extends BaseView {
    void setDataSubCategorie(List<SubCategorie> subCategorieList);
}
