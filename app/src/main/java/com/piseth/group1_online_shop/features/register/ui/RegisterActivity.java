package com.piseth.group1_online_shop.features.register.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.piseth.group1_online_shop.R;
import com.piseth.group1_online_shop.app.BaseActivity;
import com.piseth.group1_online_shop.features.register.presenter.RegisterPresenter;
import com.piseth.group1_online_shop.features.register.view.RegisterView;
import com.piseth.group1_online_shop.models.req.RegisterReq;

public class RegisterActivity extends BaseActivity implements RegisterView {
    private EditText etUsername,etPhone,etPassword, etConfirmPassword, etEmail;
    private Button btnRegister;
    private RegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        initView();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString();
                String email = etEmail.getText().toString();
                String phone = etPhone.getText().toString();
                String pass = etPassword.getText().toString();
                String confirm = etConfirmPassword.getText().toString();
                if(!pass.equals(confirm)){
                   showToastMessage("Confirm password not match!");
                   return;
                }
                RegisterReq req = new RegisterReq();
                req.setUsername(username);
                req.setEmail(email);
                req.setPassword(pass);
                req.setPhone(phone);
                registerPresenter.register(req);
            }
        });
    }

    private void initView(){
        registerPresenter = new RegisterPresenter(this);
        etUsername = findViewById(R.id.etUsername);
        etPhone = findViewById(R.id.etPhone);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword =findViewById(R.id.etConfirmPassword);
        etEmail = findViewById(R.id.etEmail);
        btnRegister = findViewById(R.id.btnRegister);
    }

    @Override
    public void onLoading(String message) {
        showToastMessage(message);
    }

    @Override
    public void onHideLoading(String message) {
        showToastMessage(message);
    }

    @Override
    public void onError(String message) {
        showToastMessage(message);
    }

    @Override
    public void onSuccess(String message) {
        showToastMessage(message);
        finish();
    }

    @Override
    public void onInternalServerError(String message) {
        showToastMessage(message);
    }

    private void showToastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_LONG).show();
    }
}