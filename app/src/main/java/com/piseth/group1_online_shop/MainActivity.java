package com.piseth.group1_online_shop;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.piseth.group1_online_shop.adapters.CategoryAdapter;
import com.piseth.group1_online_shop.adapters.SlideAdapter;
import com.piseth.group1_online_shop.app.BaseActivity;
import com.piseth.group1_online_shop.features.home.HomePresentor;
import com.piseth.group1_online_shop.features.home.HomeView;
import com.piseth.group1_online_shop.features.register.ui.LoginActivity;
import com.piseth.group1_online_shop.features.register.ui.RegisterActivity;
import com.piseth.group1_online_shop.models.res.Category;
import com.piseth.group1_online_shop.models.res.Slide;
import com.piseth.group1_online_shop.utils.local.userSharePreference;

import java.util.List;

public class MainActivity extends BaseActivity implements HomeView {
    private Button btnOpenRegister;
    private Button btnOpenLogin;
    private RecyclerView recyclerViewCategorie, recylerViewSlide;
    private ProgressBar progressBar;
    private CategoryAdapter categoryAdapter;
    private HomePresentor homePresentor;
    private SlideAdapter slideAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar=findViewById(R.id.progressBar);
        recyclerViewCategorie=findViewById(R.id.recyclerViewCategories);
        recylerViewSlide=findViewById(R.id.slideView);
        btnOpenLogin=findViewById(R.id.btnOpenLogin);
        btnOpenRegister=findViewById(R.id.btnOpenRegister);
        homePresentor=new HomePresentor(this);
        homePresentor.getAllSlide();
        //slide promotions
        //login button
        btnOpenLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!"".equals(userSharePreference.getUser(MainActivity.this).getUsername())){
                    userSharePreference.removeUser(MainActivity.this);
                }
                Intent intent=new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        //register button
        btnOpenRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!"".equals(userSharePreference.getUser(this).getUsername())){
            btnOpenLogin.setText("Log out");
            btnOpenRegister.setVisibility(View.GONE);
        }else {
            btnOpenLogin.setText("Login");
            btnOpenRegister.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onLoading(String message) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading(String message) {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onError(String message) {
        showMessage(message);
    }

    @Override
    public void onSuccess(String message) {
        showMessage(message);
    }

    @Override
    public void onInternalServerError(String message) {
        showMessage(message);
    }

    @Override
    public void setCategoryList(List<Category> categoryList) {
        if (!categoryList.isEmpty()) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
            categoryAdapter = new CategoryAdapter(this, categoryList);
            recyclerViewCategorie.setLayoutManager(gridLayoutManager);
            recyclerViewCategorie.setAdapter(categoryAdapter);
        }
    }

    @Override
    public void setSlideList(List<Slide> slideList) {
        if (slideList.isEmpty()){

        }
        slideAdapter =new SlideAdapter(this,slideList);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(
                this,1,RecyclerView.HORIZONTAL,false
        );
        recylerViewSlide.setLayoutManager(gridLayoutManager);
        recylerViewSlide.setAdapter(slideAdapter);
        homePresentor.getAllCategorys();
    }
}