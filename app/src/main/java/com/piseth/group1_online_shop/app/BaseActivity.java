package com.piseth.group1_online_shop.app;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();
    }
    protected void showMessage(String meassage){
        Toast.makeText(this, meassage, Toast.LENGTH_SHORT).show();
    }
}
