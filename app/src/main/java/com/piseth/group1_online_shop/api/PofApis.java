package com.piseth.group1_online_shop.api;


import com.piseth.group1_online_shop.models.User;
import com.piseth.group1_online_shop.models.req.LoginReq;
import com.piseth.group1_online_shop.models.req.RegisterReq;
import com.piseth.group1_online_shop.models.res.Category;
import com.piseth.group1_online_shop.models.res.RegisterRes;
import com.piseth.group1_online_shop.models.res.Slide;
import com.piseth.group1_online_shop.models.res.SubCategorie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PofApis {

    @POST("/api/oauth/register")
    Call<RegisterRes> authRegister(@Body RegisterReq req);
    @POST("/api/oauth/token")
    Call<User> authLogin(@Body LoginReq req);
    @GET("/api/public/category/list")
    Call<List<Category>> getAllCategory();
    @GET("/api/public/sub-category/list/{id}")
    Call<List<SubCategorie>> getAllSubCategories(@Path("id") Integer id);
    @GET("/api/app/slide")
    Call<List<Slide>> getAllSlide();
}
