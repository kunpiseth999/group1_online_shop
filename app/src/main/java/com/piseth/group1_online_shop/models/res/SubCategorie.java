package com.piseth.group1_online_shop.models.res;

import com.google.gson.annotations.SerializedName;

public class SubCategorie{

	@SerializedName("nameKh")
	private String nameKh;

	@SerializedName("imageUrl")
	private Object imageUrl;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("categoryId")
	private int categoryId;

	@SerializedName("status")
	private String status;

	public void setNameKh(String nameKh){
		this.nameKh = nameKh;
	}

	public String getNameKh(){
		return nameKh;
	}

	public void setImageUrl(Object imageUrl){
		this.imageUrl = imageUrl;
	}

	public Object getImageUrl(){
		return imageUrl;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCategoryId(int categoryId){
		this.categoryId = categoryId;
	}

	public int getCategoryId(){
		return categoryId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SubCategorie{" + 
			"nameKh = '" + nameKh + '\'' + 
			",imageUrl = '" + imageUrl + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",categoryId = '" + categoryId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}