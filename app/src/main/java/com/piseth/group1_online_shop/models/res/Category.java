package com.piseth.group1_online_shop.models.res;

import com.google.gson.annotations.SerializedName;

public class Category{

	@SerializedName("nameKh")
	private String nameKh;

	@SerializedName("imageUrl")
	private String imageUrl;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private String status;

	public String getNameKh(){
		return nameKh;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	public String getStatus(){
		return status;
	}
}