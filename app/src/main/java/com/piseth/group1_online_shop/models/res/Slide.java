package com.piseth.group1_online_shop.models.res;

import com.google.gson.annotations.SerializedName;

public class Slide {

	@SerializedName("slidOrder")
	private int slidOrder;

	@SerializedName("imageUrl")
	private String imageUrl;

	@SerializedName("id")
	private int id;

	@SerializedName("status")
	private String status;

	public void setSlidOrder(int slidOrder){
		this.slidOrder = slidOrder;
	}

	public int getSlidOrder(){
		return slidOrder;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SlideItem{" + 
			"slidOrder = '" + slidOrder + '\'' + 
			",imageUrl = '" + imageUrl + '\'' + 
			",id = '" + id + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}