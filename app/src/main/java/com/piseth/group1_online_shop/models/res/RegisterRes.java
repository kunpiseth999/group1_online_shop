package com.piseth.group1_online_shop.models.res;

import com.google.gson.annotations.SerializedName;

public class RegisterRes{

	@SerializedName("data")
	private Object data;

	@SerializedName("message")
	private String message;

	public void setData(Object data){
		this.data = data;
	}

	public Object getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"RegisterRes{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}