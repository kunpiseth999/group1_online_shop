package com.piseth.group1_online_shop.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.piseth.group1_online_shop.R;
import com.piseth.group1_online_shop.features.categorie.sub.SubCategorieActivity;
import com.piseth.group1_online_shop.models.res.Category;
import com.piseth.group1_online_shop.models.res.SubCategorie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private Context context;
    private List<Category> categoryList;

    public CategoryAdapter(Context context, List<Category> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom_categories_item,null,false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category=categoryList.get(position);
        if (! category.getName().equals("") || null !=category.getName()){
            holder.CategoryName.setText(category.getName());
        }
        Picasso.get().load(category.getImageUrl()).into(holder.CategoryImg);
        holder.CategoryImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, SubCategorieActivity.class);
                intent.putExtra("ID",category.getId());
                context.startActivity(intent);
            }
        });
        holder.CategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, SubCategorieActivity.class);
                intent.putExtra("ID",category.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (!categoryList.isEmpty()){
            return categoryList.size();
        }
        return 0;
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView CategoryImg;
        TextView CategoryName;
        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            CategoryImg=itemView.findViewById(R.id.categorieImg);
            CategoryName=itemView.findViewById(R.id.categorieName);
        }
    }
}
