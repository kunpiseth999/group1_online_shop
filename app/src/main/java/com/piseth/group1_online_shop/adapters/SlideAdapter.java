package com.piseth.group1_online_shop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.piseth.group1_online_shop.models.res.Slide;
import com.piseth.group1_online_shop.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SlideAdapter extends RecyclerView.Adapter<SlideAdapter.SlideViewHolder>{
    private Context context;
    private List<Slide> list;

    public SlideAdapter(Context context, List<Slide> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public SlideViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(
               R.layout.card_item_slide_layout ,null,false
        );
        return new SlideViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SlideViewHolder holder, int position) {
        Slide slide=list.get(position);
        Picasso.get().load(slide.getImageUrl()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        if (list.isEmpty()){
            return 0;
        }
        return list.size();
    }

    public static class SlideViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        public SlideViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.imageSlideView);
        }
    }
}
