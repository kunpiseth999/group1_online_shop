package com.piseth.group1_online_shop.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.piseth.group1_online_shop.R;
import com.piseth.group1_online_shop.models.res.SubCategorie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class subCategoryAdapter extends RecyclerView.Adapter<subCategoryAdapter.subCategoryViewHolder> {
    private Context context;
    private List<SubCategorie> categoryList;

    public subCategoryAdapter(Context context, List<SubCategorie> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public subCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom_sub_categories_item,null,false);
        return new subCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull subCategoryViewHolder holder, int position) {
        SubCategorie category=categoryList.get(position);
        if (! category.getName().equals("") || null !=category.getName()){
            holder.CategoryName.setText(category.getName());
        }
        Picasso.get().load((Uri) category.getImageUrl()).into(holder.CategoryImg);
    }

    @Override
    public int getItemCount() {
        if (!categoryList.isEmpty()){
            return categoryList.size();
        }
        return 0;
    }

    public static class subCategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView CategoryImg;
        TextView CategoryName;
        public subCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            CategoryImg=itemView.findViewById(R.id.categorieImg);
            CategoryName=itemView.findViewById(R.id.categorieName);
        }
    }
}
